# Object Detection Tensorflowlite
![Upwork Badge](https://img.shields.io/badge/Upwork-6FDA44?logo=upwork&logoColor=fff&style=plastic) ![GitLab Badge](https://img.shields.io/badge/GitLab-FC6D26?logo=gitlab&logoColor=fff&style=plastic)

## Description
This project is about training a model using Tensorflow and convert it into TenflowLite.

## Tech and Tools
![TensorFlow Badge](https://img.shields.io/badge/TensorFlow-FF6F00?logo=tensorflow&logoColor=fff&style=plastic) ![Google Colab Badge](https://img.shields.io/badge/Google%20Colab-F9AB00?logo=googlecolab&logoColor=fff&style=plastic) ![Python Badge](https://img.shields.io/badge/Python-3776AB?logo=python&logoColor=fff&style=plastic) ![.ENV Badge](https://img.shields.io/badge/.ENV-ECD53F?logo=dotenv&logoColor=000&style=plastic)


## Tech Support
Contact me at ![Gmail Badge](https://img.shields.io/badge/Gmail-EA4335?logo=gmail&logoColor=fff&style=flat-square) pangineering@gmail.com

---

## Support Me
### 💰You can help me by becoming a sponsor on GitHub
[![GitHub Sponsor](https://img.shields.io/badge/GitHub%20Sponsors-EA4AAA?logo=githubsponsors&logoColor=fff&style=square)](https://github.com/sponsors/pangineering) 

### 💰You can help me by Donating
[![BuyMeACoffee](https://img.shields.io/badge/Buy%20Me%20a%20Coffee-ffdd00?style=for-the-badge&logo=buy-me-a-coffee&logoColor=black)](https:buymeacoffee.com/pangineering)  
[![PayPal](https://img.shields.io/badge/PayPal-00457C?style=for-the-badge&logo=paypal&logoColor=white)](https://paypal.me/pangineering)  
[![Ko-Fi](https://img.shields.io/badge/Ko--fi-F16061?style=for-the-badge&logo=ko-fi&logoColor=white)](https://ko-fi.com/pangineering) 

---

## Hire me
1. Directly via ![Gmail Badge](https://img.shields.io/badge/Gmail-EA4335?logo=gmail&logoColor=fff&style=flat-square) pangineering@gmail.com
2. Send me an invitation on [![Upwork Badge](https://img.shields.io/badge/Upwork-6FDA44?logo=upwork&logoColor=fff&style=plastic)](https://www.upwork.com/freelancers/~01178d60b545fc9aad?viewMode=1)
3. Check out my gigs on [![Fiverr Badge](https://img.shields.io/badge/Fiverr-1DBF73?logo=fiverr&logoColor=fff&style=plastic)](https://www.fiverr.com/pengineering)

