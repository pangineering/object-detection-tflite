import tensorflow as tf

# Convert the model
converter = tf.lite.TFLiteConverter.from_saved_model('./export/') # path to the SavedModel directory
converter.optimizations = [tf.lite.Optimize.DEFAULT]
tflite_quant_model = converter.convert()
# Save the model.
with open('q_model.tflite', 'wb') as f:
  f.write(tflite_model)

